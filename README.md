# README #

This is a demo for testing a stochastic optimization method for neural networks
based on the theory of successive convex approximations. The algorithm is described here:
	
	[1] Scardapane, S. and Di Lorenzo, P., 2017. Stochastic Training of Neural Networks via 
	Successive Convex Approximations. arXiv preprint arXiv:1706.04769.
	
We compare the algorithm with Adam on a simple UCI regression dataset (included). The code
is in Python 3.5, with the back-propagation step implemented in Autograd:

	https://github.com/HIPS/autograd

### How to run the demo ###

* Install autograd from the previous link.
* Run 'main.py'; all configuration is made in the initial lines of the script.
* The code for the optimizer is in the 'optimizers' module, while all code for
	building the neural network is in the 'nets' module.

### References ###

For more information on the underlying theory, please refer to:

	[2] Yang, Y., Scutari, G., Palomar, D.P. and Pesavento, M., 2016. A parallel decomposition 
	method for nonconvex stochastic multi-agent optimization problems. IEEE Transactions on 
	Signal Processing, 64(11), pp.2949-2964.
	[3] Mokhtari, A., Koppel, A., Scutari, G. and Ribeiro, A., 2017, March. Large-scale nonconvex 
	stochastic optimization by doubly stochastic successive convex approximation. In Acoustics, 
	Speech and Signal Processing (ICASSP), 2017 IEEE International Conference on (pp. 4701-4705). IEEE.
	
### Contact ###

For any comment, request or suggestion you can write me at:
	simone [dot] scardapane [at] uniroma1 [dot] it