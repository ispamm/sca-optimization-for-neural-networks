# -*- coding: utf-8 -*-

import autograd.numpy as np
import autograd.numpy.random as npr
from autograd import value_and_grad, jacobian

def init_params(layer_sizes, scale=0.1, rs=npr.RandomState(0)):
    """ 
    Initialize the parameters of the network.
    """
    return [(rs.randn(insize, outsize) * scale,   # weight matrix
                     rs.randn(outsize) * scale)           # bias vector
                     for insize, outsize in zip(layer_sizes[:-1], layer_sizes[1:])]

        
def predict(weights, inputs, nonlinearity=np.tanh):
    """
    Predict the output of the network.
    """
    for W, b in weights:
        outputs = np.dot(inputs, W) + b
        inputs = nonlinearity(outputs)
    return inputs

def forward_pass(weights, inputs):
    """
    Similar to predict, but returns the entire list of pre- and post- activations.
    Only works with tanh nonlinearities.
    """
    pre = list()
    post = list()
    for W, b in weights:
        outputs = np.dot(inputs, W) + b
        pre.append(outputs)
        inputs = np.tanh(outputs)
        post.append(inputs)
    return pre, post
        

def squared_loss(weights, inputs, targets, C, unflattener):
    """
    Regularized squared loss
    """
    return np.mean(np.square(targets.reshape(inputs.shape[0], 1) - predict(unflattener(weights), inputs))) + \
            C*np.sum(np.square(weights))
    
    
def get_grad(weights, inputs, targets, C, unflattener):
    """
    Compute the gradient of the loss function
    """
    g = value_and_grad(lambda w: squared_loss(w, inputs, targets, C, unflattener))
    return g(weights)

def get_jacobian(weights, inputs, unflattener):
    """
    Computes the weight Jacobian of the network with respect to the mini-batch.
    This is flexible but currently very slow.
    TODO: find a way to make this faster.
    """
    
    # Version 1
    #weights_unflat = unflattener(weights)
    #ji_fcn = lambda x: grad(lambda w: predict(w, x))
    #return np.asarray([flatten(ji_fcn(x)(weights_unflat))[0].T for x in inputs])
    
    # Version 2
    j_fcn = jacobian(lambda w: predict(unflattener(w), inputs))
    return j_fcn(weights)[:, 0, :]

def d_tanh(s):
    return 1 - s**2

def get_jacobian_analytical(weights, weights_flat, inputs, pre, post):
    """
    Computes the weight Jacobian without resorting to automatic differencing.
    This only works with feedforward networks with tanh nonlinearities right now.
    TODO: make this more flexible.
    """
    
    # Initialize Jacobian
    N, d = inputs.shape
    J = np.zeros((N, len(weights_flat)))

    # Current index
    idx = len(weights_flat) - 1

    # Get derivative of output
    dActFunc = d_tanh(pre.pop())

    # Outputs (unused)
    _ = post.pop()

    # Output weights
    W, b = weights.pop()

    # Outputs of last hidden layer
    postActFunc = post.pop()

    # Get Jacobian for output layer
    grad_output = postActFunc*dActFunc
    J[:, idx] = dActFunc.reshape(N)

    J[:, idx - W.size: idx] = grad_output.reshape(N, -1)
    idx -= W.size

    for _ in range(len(weights) - 1):

        # Get derivative of hidden layer
        dActFunc_hidden = d_tanh(pre.pop())

        # Post-activation of previous layer
        postActFunc = post.pop()
        
        # Propagate error
        dActFunc = np.dot(dActFunc, W.T)*dActFunc_hidden
        grad_hidden_W = dActFunc.reshape(N, 1, -1)*\
            postActFunc.reshape(N, postActFunc.shape[1], 1)
    
        # Hidden weights
        W, b = weights.pop()
    
        J[:, idx-dActFunc_hidden.shape[1]:idx] = dActFunc.reshape(N, -1)
        idx -= b.size
        J[:, idx - W.size: idx] = grad_hidden_W.reshape(N, -1)
        idx -= W.size
    
    # Get derivative of first hidden layer
    dActFunc_hidden = d_tanh(pre.pop())

    dActFunc = np.dot(dActFunc, W.T)*dActFunc_hidden
    grad_hidden_W = dActFunc.reshape(N, 1, -1)*inputs.reshape(N, d, 1)

    # Hidden weights
    W, b = weights.pop()
    
    J[:, idx-dActFunc_hidden.shape[1]:idx] = dActFunc.reshape(N, -1)
    idx -= b.size
    J[:, idx - W.size: idx] = grad_hidden_W.reshape(N, -1)
    idx -= W.size
    
    return J