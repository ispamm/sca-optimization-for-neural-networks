# -*- coding: utf-8 -*-

from sklearn import preprocessing, model_selection
import pandas as pd
import numpy as np

def load_data():
    """
    Utility function to load the Whine dataset, 
    split into a training part and a testing part and correctly normalized.
    """
    d = pd.read_csv('data/winequality-white.csv', sep=';')
    X = d.values[:, :-1]
    y = d.values[:, -1]
    X = preprocessing.Imputer(strategy='median').fit_transform(X)
    X = preprocessing.MinMaxScaler(feature_range=(-0.5, 0.5)).fit_transform(X)
    y = preprocessing.MinMaxScaler(feature_range=(-0.9, +0.9)).fit_transform(y.reshape(-1, 1))
    return model_selection.train_test_split(X, y, test_size=0.25)# -*- coding: utf-8 -*-

class MiniBatchIterator():
    """
    A class to get mini-batches sequentially.
    """
    def __init__(self, X, y, B, random_seed=1):
        self.X = X
        self.y = y
        self.B = B
        self.idx = 0
        self.indices = np.arange(self.X.shape[0])
        np.random.seed(random_seed)

    # Create new mini-batch
    def next_minibatch(self, shuffle=True):
        
        if self.B == np.inf:
            return (self.X, self.y)
        
        # Shuffle the data if we are starting a new epoch
        if self.idx == 0 and shuffle:
            np.random.shuffle(self.indices)
            
        if self.idx + self.B > self.X.shape[0]:
            end_idx = self.X.shape[0]
            reset = True
        else:
            end_idx = self.idx + self.B
            reset = False
        
        X_batch = self.X[self.indices[self.idx:end_idx]]
        y_batch = self.y[self.indices[self.idx:end_idx]]
        
        if reset:
            self.idx = 0
        else:
            self.idx += self.B
        
        return (X_batch, y_batch)