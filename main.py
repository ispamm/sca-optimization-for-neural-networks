# -*- coding: utf-8 -*-

"""
A demo showing a comparison of Adam with respect to the optimizer introduced in:
    https://arxiv.org/abs/1706.04769
    
We use a simple feedforward network with two layers. The backpropagation step is
implemented using Autograd:
    https://github.com/HIPS/autograd
"""

# Custom imports
from nets import init_params, squared_loss, get_grad
from utils import load_data, MiniBatchIterator
from optimizers import adam, sca_ridge

# Autograd imports
import autograd.numpy as np
from autograd.util import flatten

 # Other imports
import matplotlib.pyplot as plt
import pandas as pd

# Set seed for PRNG
np.random.seed(1)

# Number of runs
runs = 50

# Number of iterations
iters = 500

# Topology of the network
layers = [11, 10, 4, 1]

# Tensor to save the loss state after every iteration
loss_history = np.zeros((2, runs, iters))

for r in range(runs):
        
    print('Executing run ', r+1, ' of ', runs, '...')

    # Get data
    (X_trn, X_tst, y_trn, y_tst) = load_data()
        
    # Initialize network
    w = init_params(layers)
        
    # Flatten the parameters
    w_flat, unflattener = flatten(w)
    
    # Define a function to compute the gradient
    f_grad = lambda w, x, y: get_grad(w, x, y, 0, unflattener)
    
    for (i, opt) in zip(range(2), (adam, sca_ridge)):
            
            print('\tEvaluating ', opt.__name__, '...')
            batch_iterator = MiniBatchIterator(X_trn, y_trn, 20, random_seed=r)
            
            # Train
            w_final, loss_history[i, r, :], g = opt(f_grad, w_flat.copy(), batch_iterator, unflattener, max_it=iters)
        
            # Evaluate
            e = squared_loss(w_final, X_tst, y_tst, 0.0, unflattener)
            print('\t\tFinal error on test set: ', e)
        
# Evolution of the objective function
plt.figure()
plt.clf()
plt.xlabel('Iteration')
plt.ylabel('Objective function')
#plt.yscale('log')
plt.title('Objective function')
plt.plot(range(iters), pd.ewma(np.sum(loss_history[0,:, :], axis=0)/runs, halflife=10), label='Adam')
plt.plot(range(iters), pd.ewma(np.sum(loss_history[1,:, :], axis=0)/runs, halflife=10), label='SCA')
plt.legend(loc='upper right')
plt.grid()