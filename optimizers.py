# -*- coding: utf-8 -*-

import autograd.numpy as np
from numpy import array_split
from numpy.random import choice
from numpy.linalg import solve

from nets import get_jacobian_analytical, forward_pass

def adam(f_grad, w, batch_iterator, unflattener, max_it=1000, step_size=0.01, b1=0.9, b2=0.999, eps=10**-8):
    """
    Adam optimizer adapted from the Autograd code.
    Parameters:
        f_grad: a function returning value and gradient of the cost given the current parameters.
        w: flattened vector of parameters.
        batch_iterator: instance of MiniBatchIterator to get the mini-batches.
        unflattener: utility function to unflatten the weight vector.
        max_it: number of iterations.
        step_size/b1/b2/eps: see the Adam paper for more details.
    """
    
    m = np.zeros(len(w))
    v = np.zeros(len(w))
    err = np.zeros(max_it)
    grad = np.zeros(max_it)
    
    for t in range(max_it):
        
        (X_batch, y_batch) = batch_iterator.next_minibatch()
        
        err[t], g = f_grad(w, X_batch, y_batch)
        grad[t] = np.sum(g**2)
        
        m = (1 - b1) * g      + b1 * m  # First  moment estimate.
        v = (1 - b2) * (g**2) + b2 * v  # Second moment estimate.
        mhat = m / (1 - b1**(t + 1))    # Bias correction.
        vhat = v / (1 - b2**(t + 1))
        w = w - step_size*mhat/(np.sqrt(vhat) + eps)
        
    return w, err, grad

def sca_ridge(f_grad, w, batch_iterator, unflattener, step_size = 0.1, step_size_eps = 0.01, \
              max_it=1000, rho = 0.9, rho_eps = 0.01, C=0, blocks=1, virtual_processors=1, \
              tau=0.01):
    """
    Stochastic SCA optimizer for the ridge regression cost.
    Parameters are similar to Adam, plus:
        step_size: initial value for the learning rate.
        step_size_eps: decreasing factor for the step-size.
        rho: initial value for the mixing coefficient.
        rho_eps: decreasing factor for the mixing coefficient.
        C: weight decay amount.
        blocks: amount of blocks in which to split the weight vector.
        virtual_processors: number of parallel steps for the optimization process.
        tau: coefficient of the proximal regularizer.
    """
    
    if blocks < virtual_processors:
        blocks = virtual_processors
    
    err = np.zeros(max_it)
    grad = np.zeros(max_it)
    d = np.zeros(len(w))
    
    # Compute the partitioning of the weights
    w_idx = array_split(np.arange(len(w)), blocks)
    
    # Mask for indexing the rest of the weights
    w_not_idx = np.ones((blocks, len(w)), dtype=bool)
    for p in range(blocks):
        w_not_idx[p, w_idx[p]] = False
    
    for t in range(max_it):
        
        # Randomly sample some blocks
        blocks_t = choice(blocks, virtual_processors, replace=False)
        
        # Get mini-batch of data
        (X_batch, y_batch) = batch_iterator.next_minibatch()
            
        # Get unflattened version
        w_unflat = unflattener(w)
            
        # Run forward pass
        pre, post = forward_pass(w_unflat, X_batch)
        y_net = post[-1]
    
        # Get Jacobian (analytical)
        J = get_jacobian_analytical(w_unflat, w, X_batch, pre, post)
            
        # Compute current error
        e = y_batch - y_net
            
        # Compute loss on current iteration
        err[t] = np.mean(np.square(e)) + C*np.sum(np.square(w))
            
        # Compute gradient on current iteration
        g_loss = -2.0*np.mean(e*J, axis=0)
        grad[t] = np.sum((g_loss + C*2.0*w)**2)
        
        # Compute residuals
        r = y_batch - y_net + np.dot(J, w).reshape(-1, 1)
        
        for p in range(virtual_processors):
        
            # Get current block and indices
            block = blocks_t[p]
            idx = w_idx[block]
            
            # Compute current A block
            A_rowblock = np.dot(J[:, idx].T, J)

            # Compute J.T times r
            Jr = np.dot(J[:, idx].T, r).reshape(-1)
    
            # Compute A, b matrices
            A = (rho/len(y_batch))*A_rowblock[:, idx] + (C + tau)*np.eye(len(idx))
            b = (rho/len(y_batch))*Jr - ((1-rho)*0.5)*d[idx] + tau*w[idx] -\
                (rho/len(y_batch))*np.dot(A_rowblock[:, w_not_idx[block]], w[w_not_idx[block]])
            
            # Solve surrogate optimization
            w_hat = solve(A, b)
          
            # Update auxiliary variables
            d[idx] = (1-rho)*d[idx] + rho*g_loss[idx]
            
            # Update variable
            w[idx] = (1-step_size)*w[idx] + step_size*w_hat
    
        # Update stepsize and rho
        rho = rho*(1-rho_eps*rho)
        step_size = step_size*(1-step_size_eps*step_size)
        
    return w, err, grad